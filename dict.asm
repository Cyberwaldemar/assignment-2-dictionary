%include "lib.inc"
%define OFFSET 8
global find_word
section .text

; rdi - str pointer
; rsi - dict pointer

find_word:
    push r12
    push r13
    mov  r12, rdi
    mov  r13, rsi
    .loop:
        test rsi, rsi
        jz   .fail
        add  rsi, OFFSET    ; offset
        mov  rdi, r12
        call string_equals
        test rax, rax
        jz  .next
        mov  rax, r13
        jmp .exit
    .next:
        mov  rsi, [r13]
        mov  r13, rsi
        jmp .loop
    .fail:
        xor  rax, rax
    .exit:
        pop  r13
        pop  r12
        ret

