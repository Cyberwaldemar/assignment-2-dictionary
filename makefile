program: main.o lib.o dict.o
	ld -o $@ $^

main.o: main.asm dict.inc colon.inc lib.inc words.inc

dict.o: dict.asm lib.inc

%.o: %.asm
	nasm -felf64 -o $@ $<

clean:
	rm -rf *.o test*

test: program 
	python3 checker.py program gen.py 5

.PHONY: clean test

