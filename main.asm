%include "dict.inc"
%include "lib.inc"
%include "words.inc"
%define BUF_SZ 255
%define OFFSET 255

global _start

section .rodata
invalid_input: db "Key invalid", 0
not_found:     db "Element is not found", 0

section .bss

buffer: resb BUF_SZ

section .text

_start:
    mov  rdi, buffer
    mov  rsi, BUF_SZ
    call read_word
    test rax, rax
    jz  .invalid
    mov  rdi, buffer
    mov  rsi, setc    ; start
    call find_word
    test rax, rax
    jz  .not_found
    lea  rdi, [rax+OFFSET] ; label + offset
    call string_length
    add  rdi, rax
    inc  rdi          ; \0
    call print_string
    jmp  exit
    .invalid:
        mov  rdi, invalid_input
        call print_error
        jmp  exit
    .not_found:
        mov  rdi, not_found
        call print_error
        jmp  exit

