import os, sys

f = sys.argv[1]
os.system("make program >/dev/null")

tests = {
    "koshkodevochka": "verim verim",
    "": "Element is not found",
    "asdfasd" : "Element is not found",
    "greg": "hse ami langs labs mood",
    "koshkodevo4ka"*24: "Key invalid"
}

for i, v2 in enumerate(tests):
    print('Test', i + 1)
    with open("test-input", "w") as file:
        file.write(v2)
    v1 = os.popen(f"./{f} < test-input 2>&1").read().strip()
    if v1 != tests[v2]:
        print("Failed test:")
        print(v2)
        print(f'Output of {f}:')
        print(v1)
        print(f'Answer:')
        print(tests[v2])
        break
    print(".... + Passed\n")

