%define WRITE_SYSCALL    1
%define READ_SYSCALL     0
%define EXIT_SYSCALL     60
%define STDOUT           1
%define STDERR           2
%define STDIN            0
%define MAXLEN           20
%define RADIX10          10


global exit
global string_length
global print_error
global print_string
global print_newline
global print_char
global print_int
global print_uint
global string_equals
global read_char
global read_word
global parse_uint
global parse_int
global string_copy 

section .text
; Принимает код возврата и завершает текущий процесс
exit:
    mov rax, EXIT_SYSCALL
    syscall

; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
    xor rax, rax

    .loop:
        cmp byte[rdi + rax], 0
        je .end
        inc rax
        jmp .loop
    .end:
        ret

; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_error:
    push rdi
    call string_length
    mov rdx, rax
    mov rax, WRITE_SYSCALL
    pop rsi
    mov rdi, STDERR
    syscall
    ret

print_string:
    push rdi
    call string_length
    mov rdx, rax
    mov rax, WRITE_SYSCALL
    pop rsi
    mov rdi, STDOUT
    syscall
    ret

; Переводит строку (выводит символ с кодом 0xA)
print_newline:
    mov rdi, `\n`

; Принимает код символа и выводит его в STDOUT
print_char:
    push rdi
    mov rax, WRITE_SYSCALL
    mov rdi, STDOUT
    mov rsi, rsp
    mov rdx, 1
    syscall
    pop rdi
    ret

; Выводит знаковое 8-байтовое число в десятичном формате 
print_int:
    test rdi, rdi
    jns print_uint
    push rdi
    mov dil, '-'
    call print_char
    pop rdi
    neg rdi

; Выводит беззнаковое 8-байтовое число в десятичном формате 
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint:
    sub rsp, MAXLEN + 1
    mov rax, rdi
    mov rcx, MAXLEN
    mov r9, RADIX10
    mov byte[rsp + MAXLEN], 0
    .loop:
        xor rdx, rdx
        div r9
        add dl, '0'
        dec rcx
        mov [rsp + rcx], dl
        test rax, rax
        jne .loop
    .print:
        mov rdi, rsp
        add rdi, rcx
        call print_string
        add rsp, MAXLEN + 1
        ret

; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:
    mov rax, 1
    xor rdx, rdx
    .loop:
        mov r9b, byte[rdi + rdx]
        cmp r9b, byte[rsi + rdx]
        jne .bad
        inc rdx
        test r9b, r9b
        jne .loop
        ret

        .bad:
            xor rax, rax
            ret

; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
    dec rsp
    mov rdi, STDIN
    mov rax, READ_SYSCALL
    mov rsi, rsp
    mov rdx, 1
    syscall

    test rax, rax
    mov al, [rsp]
    jnz .exit
    xor al, al
    .exit:
        inc rsp
        ret 

; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из 0, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор

read_word:
    push r12
    push r13
    push r14
    mov r12, rdi ; buf*
    mov r13, rsi ; max length
    xor r14, r14 ; length

    .skip_space:
        call read_char
        cmp al, ' '
        je .skip_space
        cmp al, `\n`
        je .skip_space
        cmp al, `\t`
        je .skip_space
    
    .loop:
        cmp r13, r14
        je .overflow
        mov [r12 + r14], al
        inc r14
        test al, al
        je .break
        cmp al, ' '
        je .break
        cmp al, `\n`
        je .break
        cmp al, `\t`
        je .break
        call read_char
        jmp .loop
    .break:
        dec r14
        mov rax, r12
        mov rdx, r14
        jmp .exit
    .overflow:
        xor rax, rax
        xor rdx, rdx
    .exit:
        pop r14
        pop r13
        pop r12
        ret
 

; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
    xor rax, rax ; result
    xor r9, r9 ; count
    mov r10, RADIX10
    xor r11, r11 ; buf

    .next:
        mov r11b, [rdi + r9]
        cmp r11b, '0'
        jl .done
        cmp r11b, '9'
        jg .done
        inc r9
        mul r10
        add rax, r11
        sub rax, '0'
        jmp .next
    .done:
        mov rdx, r9
        ret



; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был) 
; rdx = 0 если число прочитать не удалось
parse_int:
    push r12
    push r13
    xor rax, rax
    xor r12, r12
    xor r13, r13
    mov al, [rdi]
    cmp al, '-'
    je .neg
    cmp al, '+'
    je .pos
    jmp .read
    .neg:
        inc r12
    .pos:
        inc rdi
        inc r13
    .read:
        call parse_uint
        test rax, rax
        js .bad
        test rdx, rdx
        je .exit
        add rdx, r13
        test r12, r12
        je .exit
        neg rax
    .bad 
        xor rdx, rdx
    .exit:
        pop r13
        pop r12
        ret 

; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy:
    xor rcx, rcx
    .loop:
        cmp rdx, rcx
        je .exit_out
        mov al, [rdi + rcx]
        mov [rsi + rcx], al
        inc rcx
        test al, al
        jne .loop
        mov rax, rcx
        ret
    .exit_out:
        xor rax, rax
        ret

